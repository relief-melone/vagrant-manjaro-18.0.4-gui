def refreshProxy(config)
    # Added because some proxies will respond with 407 error if a timeout after a normal call through web port is reached
    if ENV['USEPROXY'] == "true"
        config.vm.provision "shell", inline: "curl -s https://google.com"
    end 
end

Vagrant.configure("2") do |config|
    
    # VM Specs
    config.vm.provider "virtualbox" do |v|
        # Basics
        v.name = "manjaro-18.04-gui-basic"
        v.memory = 4096
        v.cpus = 2
        
        # Storage
        v.customize ["storageattach", :id, 
            "--storagectl" , "SATA Controller", 
            "--port", "1", 
            "--device", "0", 
            "--type", "dvddrive",
            "--medium", "emptydrive"
        ]

        # Grahics
        v.customize ["modifyvm", :id, "--vram", "256"]
        v.customize ["modifyvm", :id, "--accelerate3d", "on"]
        v.customize ["modifyvm", :id, "--graphicscontroller", "vboxsvga"]
        
    end    
    
    # Base Image
    config.vm.box = "reliefmelone/Manjaro-18.04-gui-de"
    config.vm.box_version = "0.0.1-beta"

    # Configure Proxy
    if ENV['USEPROXY'] == "true"
        config.vm.provision "shell", inline: "echo Configuring Proxy...."
        config.proxy.http = Secret.HTTP_PROXY
        config.proxy.https = Secret.HTTPS_PROXY
        config.proxy.no_proxy = "localhost,127.0.0.1,.bmwgroup.net,172.30.149.84,10.221.246.15"
    end

    # # Misc Config
    # refreshProxy(config)
    # config.vm.provision "file", source: "./etc/gdm/custom.conf", destination: "/tmp/gdm.custom.conf"
    # config.vm.provision "shell", inline: "cp /tmp/gdm.custom.conf /etc/gdm/custom.conf", privileged: true

    # Update
    refreshProxy(config)
    config.vm.provision "shell", inline: "echo Manjaro...."
    config.vm.provision "shell", inline: "paccache -r", privileged: true
    config.vm.provision "shell", inline: "pacman -Sy --noconfirm", privileged: true

    # Create Provisioning directory
    config.vm.provision "shell", inline: "mkdir /tmp/provisioning", privileged: true

    # Install Kubernetes
    refreshProxy(config)
    config.vm.provision "shell", inline: "echo Installing kubectl..."
    config.vm.provision "shell", path: "./installations/install-kubectl.sh", privileged: true
    
    # # Install Google Cloud SDK
    refreshProxy(config)
    config.vm.provision "shell", inline: "echo Installing Google Cloud SDK..."
    config.vm.provision "shell", path: "./installations/install-gcloud.sh", privileged: true

    # Install Openshift Command Line Tools
    refreshProxy(config)
    config.vm.provision "shell", inline: "echo Installing OpenShift Command Line Tools..."
    config.vm.provision "shell", path: "./installations/install-oc.sh", privileged: true

    # Install VS Code
    refreshProxy(config)
    config.vm.provision "shell", inline: "echo Installing VS Code..."
    config.vm.provision "shell", path: "./installations/install-vs-code.sh"

    # # Install Misc Tools
    # refreshProxy(config)
    # config.vm.provision "shell", inline: "echo Installing additional tools...."
    # config.vm.provision "shell", inline: "dnf clean all", privileged: true
    # config.vm.provision "shell", inline: "dnf -y install bind-utils", privileged: true
    # config.vm.provision "shell", inline: "dnf -y install wget", privileged: true
    # config.vm.provision "shell", inline: "dnf -y install git-all", privileged: true
    
    # Install Docker
    config.vm.provision "shell", inline: "echo Installing Docker...."
    refreshProxy(config)
    if ENV['USEPROXY'] == "true"
        # config.vm.provision "file", source: "./etc/docker/daemon.json", destination: "/tmp/daemon.json"
        config.vm.provision "file", source: "./etc/systemd/system/docker.service.d/http-proxy.conf", destination: "/tmp/systemd/http-proxy.conf"
        config.vm.provision "file", source: "./etc/systemd/system/docker.service.d/https-proxy.conf", destination: "/tmp/systemd/https-proxy.conf"
    end
    config.vm.provision "shell", path: "./installations/install-docker.sh", privileged: true

    # Install Google Chrome
    refreshProxy(config)
    config.vm.provision "shell", inline: "echo Installing Chrome...."
    config.vm.provision "shell", path: "./installations/install-google-chrome.sh", privileged: true

    # Delete Provisioning directory
    config.vm.provision "shell", inline: "rm -rf /tmp/provisioning", privileged: true

    # Display Welcome Message
    # refreshProxy(config)
    # config.vm.provision "file", source: "./welcome-message.txt", destination: "/tmp/welcome-message.txt"
    # config.vm.provision "shell", inline: "cat /tmp/welcome-message.txt"

end
