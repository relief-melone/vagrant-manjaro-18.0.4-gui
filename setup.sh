#!/bin/bash
BASEDIR=$(dirname $0)
read -n1 -p "Do you want to use a proxy? (y/n): " useProxy
echo ""

if [ "$useProxy" == "y" ]; then
  echo "Please enter the proxy host (proxy.com): "
  read proxyHost
  echo "Please enter the proxy port (8080): "
  read proxyPort
  echo "Please enter the proxy username (qq12345): "
  read proxyUser
  echo "Please enter the proxy password: "
  read proxyPassword

  export http_proxy=http://$proxyUser:$proxyPassword@$proxyHost:$proxyPort
  export https_proxy=${http_proxy}

  vagrant plugin install vagrant-proxyconf
  vagrant plugin install vagrant-secret
  vagrant secret-init

  echo HTTP_PROXY: $http_proxy
  echo HTTPS_PROXY: $https_proxy
  echo HTTP_PROXY: $http_proxy > ./.vagrant/secret.yaml
  echo HTTPS_PROXY: $https_proxy >> ./.vagrant/secret.yaml

  mkdir -p $BASEDIR/etc/systemd/system/docker.service.d
  echo "[Service]" > $BASEDIR/etc/systemd/system/docker.service.d/http-proxy.conf
  echo "Environment=HTTP_PROXY=http://$proxyUser:$proxyPassword@$proxyHost:$proxyPort" >> $BASEDIR/etc/systemd/system/docker.service.d/http-proxy.conf

  echo "[Service]" > $BASEDIR/etc/systemd/system/docker.service.d/https-proxy.conf
  echo "Environment=HTTPS_PROXY=http://$proxyUser:$proxyPassword@$proxyHost:$proxyPort" >> $BASEDIR/etc/systemd/system/docker.service.d/http-proxy.conf

  USEPROXY=true vagrant up
else
  vagrant up
fi
