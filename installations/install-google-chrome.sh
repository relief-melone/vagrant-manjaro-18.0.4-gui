mkdir /tmp/provisioning/google-chrome
git clone https://aur.archlinux.org/google-chrome.git /tmp/provisioning/google-chrome
cd /tmp/provisioning/google-chrome
makepkg -s --noconfirm
packman -U --noconfirm $(ls *.xz)
google-chrome-stable --version