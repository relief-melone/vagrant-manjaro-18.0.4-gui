mkdir /tmp/provisioning/vs-code
pacman -S --noconfirm
git clone https://AUR.archlinux.org/visual-studio-code-bin.git /tmp/provisioning/vs-code
cd /tmp/provisioning/vs-code
makepkg -s --noconfirm
pacman -U --noconfirm visual-studio-code-bin-*.pkg.tar.xz
